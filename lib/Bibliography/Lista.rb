class Lista 
  attr_reader :cabeza
  include Enumerable
  def initialize(nodes)
    nodes.each do |i| 
      raise unless i.is_a? Node
    end
    
    j = nodes.length
    j -= 1
    @cabeza = nodes[0]
    
    if j > 1
      @cabeza.next = nodes[1]
      for i in 1..j do
        nodes[i].next = nodes[i+1]
      end
    end

  end
  
  def pop
    raise if @cabeza.next == nil
    
    backup = @cabeza.value
    @cabeza = @cabeza.next
    backup
  end
  
  def push(node)
    raise unless node.is_a? Node
    actual = @cabeza
    while actual.next != nil do
      actual = actual.next
    end
    actual.next = node
  end
  
  def push_much(nodes)
    raise unless nodes.is_a? Array
    for i in nodes do
      raise unless i.is_a? Node
    end
    actual = @cabeza
    while actual.next != nil do
      actual = actual.next
    end
    j = nodes.length
    j -= 1
    for i in 0..j do
      actual.next = nodes[i]
      actual = actual.next
    end
  end
  
  def pop_much(number)
    raise unless number.is_a? Integer
    array = []
    for i in 0..number do
      array[i] = @cabeza
      @cabeza = cabeza.next
    end
    array
  end
  
  def toTail
    actual = @cabeza
    
    while actual.next != nil do
      actual = actual.next
    end
    actual
  end
  
  def each
     actual = @cabeza
     yield actual
     while actual.next != nil do
        yield actual
        actual = actual.next
     end
  end
  
  def to_s
     y="" 
     self.each{ |x| y+=x.value.to_s }
     y
  end
end