describe Lista do
  before :each do
    @var1 = Bibliography.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide",
    "The Facets of Ruby","Pragmatic Bookshelf",4,"July 7, 2013",[9781937785499,1937785491])
    @var2 = Bibliography.new(["Scott Chacon"],"Pro Git 2009th Edition","Pro","Apress",2009,"August 27, 2009",[9781430218333,1430218339])
    @var3 = Bibliography.new(["David Flanagan", "Yukihiro Matsumoto"],"The Ruby Programming Language","O'Reilly Media",1,"February 4, 2008",[1596516177,9780596516178])
    @var4 = Bibliography.new(["David Chelimsky", "Dave Astels", "Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy"],"The RSpec Book: Behaviour Driven Development with RSpec,Cucumber, and Friends","The Facets of Ruby","Pragmatic Bookshelf",1,"December 25, 2010",[1934356379,9781934356371])
    @var5 = Bibliography.new(["Richard E"], "Silverman Git Pocket Guide","O'Reilly Media",1,"(August 2, 2013)",[1449325866,9781449325862])
    @node1 = Node.new(@var1,nil)
    @node2 = Node.new(@var2,nil)
    @node3 = Node.new(@var3,nil)
    @node4 = Node.new(@var4,nil)
    @node5 = Node.new(@var5,nil)
    @node6 = Node.new(@var1,nil)
    @b1 = Lista.new([@node1,@node2,@node3])
    @b2 = Lista.new([@node4])
    @b3 = Lista.new([@node6])
  end
  

  
  describe "Se extrae el primer elemento de la lista" do
    it "Podemos hacer un pop y equivale a node1" do
      expect(@b1.pop.to_s).to eq "Dave Thomas, Andy Hunt, Chad Fowler\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide\nThe Facets of Ruby\nPragmatic Bookshelf; 4 edicion (July 7, 2013)\nISBN-13: 9781937785499\nISBN-10: 1937785491\n"
    end
    
    it "Si la extracción ha sido exitosa, ahora deberia salir node2" do
      @b1.pop
      expect(@b1.pop.to_s).to eq "Scott Chacon\nPro Git 2009th Edition\nPro\nApress; 2009 edicion (August 27, 2009)\nISBN-13: 9781430218333\nISBN-10: 1430218339\n"
    end
  end
  
  describe "Se puede insertar un elemento" do
     it "Existe un metodo para realizar la insercion simple" do
       expect(@b1).to respond_to(:push)
     end
     it "Solo permite insertar nodos" do
       expect { @b1.push(1) }.to raise_error
     end
     it "Inserta un elemento" do
       @b2.push(@node5)
       expect(@b2.cabeza.next).to eq(@node5)
       
     end
  end
  describe "Se pueden insertar varios elementos" do
    it "Existe un metodo para realizar la insercion multiple" do
       expect(@b1).to respond_to(:push_much)
     end
     it "Solo permite insertar nodos" do
       expect { @b2.push_much([@node2,@node3,1]) }.to raise_error
     end
     it "Inserta varios elementos" do
       @b2.push_much([@node2,@node3])
       @b2.pop
       expect(@b2.cabeza).to eq(@node2)
       expect(@b2.cabeza.next).to eq(@node3)
     end   
  end
  
  describe "Debe existir una Lista con su cabeza" do
    it "La lista no puede quedar vacia" do
      expect { @b3.pop }.to raise_error
    end
  end
  
  describe "Permite hacer pop multiple" do
    it "Se extraen dos nodos comprobar primero" do
      expect(@b1.pop_much(2)[0].value.to_s).to eq "Dave Thomas, Andy Hunt, Chad Fowler\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide\nThe Facets of Ruby\nPragmatic Bookshelf; 4 edicion (July 7, 2013)\nISBN-13: 9781937785499\nISBN-10: 1937785491\n"
    end
    it "Se extraen dos nodos, comprobar segundo" do
      expect(@b1.pop_much(2)[1].value.to_s).to eq "Scott Chacon\nPro Git 2009th Edition\nPro\nApress; 2009 edicion (August 27, 2009)\nISBN-13: 9781430218333\nISBN-10: 1430218339\n"
    end
    it "No acepta no numeros como parametros" do
      expect { @b1.pop_much('a') }.to raise_error
    end
  end
  
  describe "Debe haber un metodo para recorrer la lista" do
    it "Recorre la lista 1 sin problema" do
      expect(@b1.toTail).to eq @node3
    end
  end

  describe "Propiedad de enumerable" do
    it "Se puede recorrer con un each" do
      @b1.each{|i| i}
    end
  end
  describe "Se puede recorrer la lista con el each" do
    it "Se muestra la salida formateada" do
      expect(@b3.to_s).to eq(@node6.value.to_s)
    end
  end  

end